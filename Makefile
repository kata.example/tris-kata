up:
	docker-compose up -d

down:
	docker-compose down

bash_php:
	docker-compose exec --workdir=/var/www/html/tris php bash

bash_mysql:
	docker-compose exec mysql bash

rebuild:
	docker-compose up --force-recreate -d --build php
