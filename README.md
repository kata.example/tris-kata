# Tris Kata

## Dalla macchina host

 - `make` per partire (*docker-compose up -d*)
 - `make bash_php` per entrare nel container php
 - `./bin/console doctrine:migrations:migrate` al primo accesso per costruire il database

## Dal container

 - `make` per lanciare i test

## Coverage

 - `make coverage` all'interno del container

## Come giocare

Ci sono due sole rotte per giocare a questo gioco:

 - POST /start
  - crea una sorta di id di gioco usato nelle mosse successive per identificare la partita
 - POST /move
  - esegue una mossa in una determinata partita

### Iniziare una partita

 - viene creato un rigo nella tabella games
 - tutte le mosse successive useranno questo id

`curl -vX POST http://localhost/move -d '{"game_id":"95fpui4qn2ym6a4b2vb91m8mzttmy7b3","position":0,"player_number":1}'`

#### Request

    > POST /start HTTP/1.1
    > Host: localhost
    > User-Agent: curl/7.64.1
    > Accept: */*

#### Response

    < HTTP/1.1 201 Created
    < Date: Sun, 15 Aug 2021 21:13:40 GMT
    < Server: Apache/2.4.38 (Debian)
    < X-Powered-By: PHP/8.0.2
    < Cache-Control: no-cache, private
    < X-Robots-Tag: noindex
    < Content-Length: 46
    < Content-Type: application/json
    {
        "game_id":"95fpui4qn2ym6a4b2vb91m8mzttmy7b3"
    }

### La prima mossa

`curl -vX POST http://localhost/move -d '{"game_id":"95fpui4qn2ym6a4b2vb91m8mzttmy7b3","position":0,"player_number":1}'`

#### Request

    > POST /move HTTP/1.1
    > Host: localhost
    > User-Agent: curl/7.64.1
    > Accept: */*
    > Content-Length: 77
    > Content-Type: application/x-www-form-urlencoded

#### Response

    < HTTP/1.1 200 OK
    < Date: Sun, 15 Aug 2021 21:17:21 GMT
    < Server: Apache/2.4.38 (Debian)
    < X-Powered-By: PHP/8.0.2
    < Cache-Control: no-cache, private
    < X-Robots-Tag: noindex
    < Content-Length: 71
    < Content-Type: application/json
    {
        "board":[1,null,null,null,null,null,null,null,null],
        "winner":"nobody"
    }

### Provare a giocare in una posizione non libera

`curl -vX POST http://localhost/move -d '{"game_id":"95fpui4qn2ym6a4b2vb91m8mzttmy7b3","position":0,"player_number":1}'`

#### Request

    > POST /move HTTP/1.1
    > Host: localhost
    > User-Agent: curl/7.64.1
    > Accept: */*
    > Content-Length: 77
    > Content-Type: application/x-www-form-urlencoded

#### Response

    < HTTP/1.1 400 Bad Request
    < Date: Sun, 15 Aug 2021 21:20:38 GMT
    < Server: Apache/2.4.38 (Debian)
    < X-Powered-By: PHP/8.0.2
    < Cache-Control: no-cache, private
    < X-Robots-Tag: noindex
    < Content-Length: 42
    < Connection: close
    < Content-Type: application/json
    {"message":"position is already occupied"}

### Doppia mossa dello stesso giocatore

`curl -vX POST http://localhost/move -d '{"game_id":"95fpui4qn2ym6a4b2vb91m8mzttmy7b3","position":1,"player_number":1}'`

#### Request

    > POST /move HTTP/1.1
    > Host: localhost
    > User-Agent: curl/7.64.1
    > Accept: */*
    > Content-Length: 77
    > Content-Type: application/x-www-form-urlencoded

#### Response

    < HTTP/1.1 400 Bad Request
    < Date: Sun, 15 Aug 2021 21:23:00 GMT
    < Server: Apache/2.4.38 (Debian)
    < X-Powered-By: PHP/8.0.2
    < Cache-Control: no-cache, private
    < X-Robots-Tag: noindex
    < Content-Length: 41
    < Connection: close
    < Content-Type: application/json
    {"message":"same player cant move twice"}

### Proseguire la partita fino alla fine

#### Requests

`curl -vX POST http://localhost/move -d '{"game_id":"95fpui4qn2ym6a4b2vb91m8mzttmy7b3","position":4,"player_number":2}'`

`curl -vX POST http://localhost/move -d '{"game_id":"95fpui4qn2ym6a4b2vb91m8mzttmy7b3","position":1,"player_number":1}'`

`curl -vX POST http://localhost/move -d '{"game_id":"95fpui4qn2ym6a4b2vb91m8mzttmy7b3","position":3,"player_number":2}'`

`curl -vX POST http://localhost/move -d '{"game_id":"95fpui4qn2ym6a4b2vb91m8mzttmy7b3","position":2,"player_number":1}'`

#### Response

    {
        "board":[1,1,1,2,2,null,null,null,null],
        "winner":"player 1"
    }

### Matrice di gioco

La matrice di gioco viene rappresentata da un array php:

     0 | 1 | 2
    ---+---+---
     3 | 4 | 5
    ---+---+---
     6 | 7 | 8

Nell'esempio, il giocatore uno vince occupando la prima riga

     1 | 1 | 1
    ---+---+---
     2 | 2 | -
    ---+---+---
     - | - | -

### Gioco finito

Qualora si volesse provare a fare una nuova mossa quando il gioco ha gia' un vincitore, ... 

#### Request

`curl -vX POST http://localhost/move -d '{"game_id":"95fpui4qn2ym6a4b2vb91m8mzttmy7b3","position":8,"player_number":2}'`

#### Response

    < HTTP/1.1 400 Bad Request
    < Date: Sun, 15 Aug 2021 22:36:09 GMT
    < Server: Apache/2.4.38 (Debian)
    < X-Powered-By: PHP/8.0.2
    < Cache-Control: no-cache, private
    < X-Robots-Tag: noindex
    < Content-Length: 28
    < Connection: close
    < Content-Type: application/json
    {
        "message":"game is closed"
    } 
