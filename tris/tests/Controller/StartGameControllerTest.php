<?php

namespace App\Tests;

class StartGameControllerTest extends SwitchoTest
{
    public function testResponseContainsJustOneValue(): void
    {
        $this->client->request('POST', '/start');

        $this->assertResponseIsSuccessful();

        $response = $this->client->getResponse();
        $content = $response->getContent();

        $this->assertEquals(1, count(json_decode($content, true)));
        $this->assertEquals(201, $response->getStatusCode());
    }

    public function testResponseContainsId(): void
    {
        $this->client->request('POST', '/start');

        $this->assertResponseIsSuccessful();

        $response = $this->client->getResponse();
        $content = $response->getContent();

        $this->assertTrue(isset(json_decode($content, true)['game_id']));
    }

    public function testIdLengthIsEqualTo32(): void
    {
        $this->client->request('POST', '/start');

        $this->assertResponseIsSuccessful();

        $response = $this->client->getResponse();
        $content = $response->getContent();

        $this->assertEquals(32, strlen(json_decode($content, true)['game_id']));
    }

    public function testContainsNoGamesInDatabaseBeforeGameStartIsCalled(): void
    {
        $stmt = $this->connection->prepare('select count(*) num from games;');
        $stmt->execute();
        $this->assertEquals('0', $stmt->fetchAssociative()['num']);
    }

    public function testContainsAGamesInDatabaseAfterGameStartIsCalled(): void
    {
        $this->client->request('POST', '/start');

        $stmt = $this->connection->prepare('select count(*) num from games;');
        $stmt->execute();
        $this->assertEquals('1', $stmt->fetchAssociative()['num']);
    }

    public function testNGameExistsAfterNGameStart(): void
    {
        $n = rand(11, 22);

        for ($start = 0; $start < $n; $start++) {
            $this->client->request('POST', '/start');
        }

        $stmt = $this->connection->prepare('select count(*) num from games;');
        $stmt->execute();
        $this->assertEquals($n, $stmt->fetchAssociative()['num']);
    }

    // @todo check if more games can be played at the same time
}
