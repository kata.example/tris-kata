<?php

namespace App\Tests;

use App\Objects\Board;

class MoveControllerTest extends SwitchoTest
{
    /** @test */
    public function cantMoveIfGameNotExistsYet(): void
    {
        $this->client->request(
            'POST',
            '/move',
            [ ],
            [
                'headers' => [
                    'Content-type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ],
            [ ],
            json_encode([
                'game_id' => '12345678901234567890123456789012',
                'player_number' => 1,
                'position' => 1,
            ])
        );
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('game id not found', $content['message']);
        $this->assertEquals($this->client->getResponse()->getStatusCode(), 400);
    }

    public function testEmptyResponseReturns400(): void
    {
        $this->client->request(
            'POST',
            '/move',
            [ ],
            [
                'headers' => [
                    'Content-type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ],
            [ ],
            null
        );
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('empty request', $content['message']);
        $this->assertEquals($this->client->getResponse()->getStatusCode(), 400);
    }

    public function testInvalidRrequestRetunrs400(): void
    {
        $this->client->request(
            'POST',
            '/move',
            [ ],
            [
                'headers' => [
                    'Content-type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ],
            [ ],
            json_encode([
                'invalid' => 'request',
                'player_number' => 1,
                'position' => 3,
            ])
        );
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('game_id is missing', $content['message']);
        $this->assertEquals($this->client->getResponse()->getStatusCode(), 400);
    }

    /** @test */
    public function wrongGameIdReturns400(): void
    {
        $this->client->request(
            'POST',
            '/move',
            [ ],
            [
                'headers' => [
                    'Content-type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ],
            [ ],
            json_encode([
                'game_id' => 'too short id',
                'player_number' => 42,
                'position' => 2,
            ])
        );
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('game_id is too short', $content['message']);
        $this->assertEquals(
            $this->client->getResponse()->getStatusCode(),
            400
        );
    }

    /** @test */
    public function validRequestRequirePublicIdAndPlayerNumber(): void
    {
        $this->client->request(
            'POST',
            '/move',
            [ ],
            [
                'headers' => [
                    'Content-type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ],
            [ ],
            json_encode([
                'game_id' => '12345678901234567890123456789012',
                'position' => 4,
            ])
        );
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('player number is missing', $content['message']);
        $this->assertEquals(
            $this->client->getResponse()->getStatusCode(),
            400
        );
    }

    /** @test */
    public function validRequestMustcontainValidPLayerNumber(): void
    {
        $this->client->request(
            'POST',
            '/move',
            [ ],
            [
                'headers' => [
                    'Content-type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ],
            [ ],
            json_encode([
                'game_id' => '12345678901234567890123456789012',
                'player_number' => 42,
                'position' => 1,
            ])
        );
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('player number can be 1 or 2', $content['message']);
        $this->assertEquals(
            $this->client->getResponse()->getStatusCode(),
            400
        );
    }

    /** @test */
    public function moveWithGameAndPlayerNumberRequirePosition(): void
    {
        $this->client->request(
            'POST',
            '/move',
            [ ],
            [
                'headers' => [
                    'Content-type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ],
            [ ],
            json_encode([
                'game_id' => '12345678901234567890123456789012',
                'player_number' => 2,
            ])
        );
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('position is missing', $content['message']);
        $this->assertEquals($this->client->getResponse()->getStatusCode(), 400);
    }

    /** @test */
    public function completeRequestReturns200(): void
    {
        $gameId = 'abc4567890123456789012';
        $this->games->persistGame(new Board($gameId));
        $this->client->request(
            'POST',
            '/move',
            [ ],
            [
                'headers' => [
                    'Content-type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ],
            [ ],
            json_encode([
                'game_id' => $gameId,
                'player_number' => 1,
                'position' => 1,
            ])
        );
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals($this->client->getResponse()->getStatusCode(), 200);
    }

    /** @test */
    public function responseShouldIncludeDataStructure(): void
    {
        $gameId = '1234567890123456789012';
        $this->games->persistGame(new Board($gameId));
        $this->client->request(
            'POST',
            '/move',
            [ ],
            [
                'headers' => [
                    'Content-type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ],
            [ ],
            json_encode([
                'game_id' => $gameId,
                'player_number' => 1,
                'position' => 1,
            ])
        );

        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertTrue(isset($content['board']));
    }

    /** @test */
    public function boardInResponseMustContainsNineFields(): void
    {
        $gameId = '1234567890123456789012';
        $this->games->persistGame(new Board($gameId));
        $this->client->request(
            'POST',
            '/move',
            [ ],
            [
                'headers' => [
                    'Content-type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ],
            [ ],
            json_encode([
                'game_id' => $gameId,
                'player_number' => 1,
                'position' => 1,
            ])
        );

        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertTrue(count($content['board']) === 9);

        $this->assertEquals([
            'board' => [
                null,
                1,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
            ],
            'winner' => 'nobody',
        ], $content);
    }

    /** @test */
    public function validMoveIsStoredInDatabase(): void
    {
        $gameId = '1234567890123456789012';
        $this->games->persistGame(new Board($gameId));
        $this->client->request(
            'POST',
            '/move',
            [ ],
            [
                'headers' => [
                    'Content-type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ],
            [ ],
            json_encode([
                'game_id' => $gameId,
                'player_number' => 1,
                'position' => 1,
            ])
        );
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals(
            $this->client->getResponse()->getStatusCode(),
            200
        );

        // one line in moves
        $stmt = $this->connection->prepare('select count(*) as num from moves where game_id = :board');
        $stmt->bindValue('board', $gameId);
        $stmt->execute();
        $response = $stmt->fetchAssociative();
        $this->assertEquals(1, $response['num']);

        // one line in games
        $numberOfGames = $this->games->numOfRowByBoard($gameId);
        $this->assertEquals(1, $numberOfGames);
    }

    /** @test */
    public function displayAllMoves(): void
    {
        $gameId = '1234567890123456789012';
        $this->games->persistGame(new Board($gameId));

        $this->client->request(
            'POST',
            '/move',
            [ ],
            [
                'headers' => [
                    'Content-type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ],
            [ ],
            json_encode([
                'game_id' => $gameId,
                'player_number' => 1,
                'position' => 1,
            ])
        );

        $this->client->request(
            'POST',
            '/move',
            [ ],
            [
                'headers' => [
                    'Content-type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ],
            [ ],
            json_encode([
                'game_id' => $gameId,
                'player_number' => 2,
                'position' => 5,
            ])
        );

        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertTrue(count($content['board']) === 9);

        $this->assertEquals([
            'board' => [
                null,
                1,
                null,
                null,
                null,
                2,
                null,
                null,
                null,
            ],
            'winner' => 'nobody',
        ], $content);
    }

    /** @test */
    public function displayWinnerWheneverExists(): void
    {
        $gameId = '1234567890123456789012';
        $this->games->persistGame(new Board($gameId));

        $server = [
            'headers' => [
                'Content-type' => 'application/json',
                'Accept' => 'application/json',
            ]
        ];

        $moves = [[1, 1], [2, 3], [1, 0], [2, 4], [1, 2]];

        foreach ($moves as $move) {
            $request = json_encode([
                'game_id' => $gameId,
                'player_number' => $move[0],
                'position' => $move[1],
            ]);

            $this->client->request('POST', '/move', [ ], $server, [ ], $request);
        }


        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertTrue(count($content['board']) === 9);

        $this->assertEquals([
            'board' => [
                1,
                1,
                1,
                2,
                2,
                null,
                null,
                null,
                null,
            ],
            'winner' => 'player 1',
        ], $content);
    }

    /** @test */
    public function cantPlayAfterMatchHaveAWinner(): void
    {
        $server = [
            'headers' => [
                'Content-type' => 'application/json',
                'Accept' => 'application/json',
            ]
        ];

        $moves = [[1, 1], [2, 3], [1, 0], [2, 4], [1, 2], [2, 5]];

        foreach ($moves as $move) {
            $request = json_encode([
                'game_id' => '12345678901234567890123456789012',
                'player_number' => $move[0],
                'position' => $move[1],
            ]);

            $this->client->request('POST', '/move', [ ], $server, [ ], $request);
        }


        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('game should start from player 1', $content['message']);
        $this->assertEquals($this->client->getResponse()->getStatusCode(), 400);
    }

    /** @test */
    public function cantPlacePieceOnTopOfAnotherPlayerPiece(): void
    {
        $server = [
            'headers' => [
                'Content-type' => 'application/json',
                'Accept' => 'application/json',
            ]
        ];

        $moves = [[1, 1], [2, 1]];

        foreach ($moves as $move) {
            $request = json_encode([
                'game_id' => '12345678901234567890123456789012',
                'player_number' => $move[0],
                'position' => $move[1],
            ]);

            $this->client->request('POST', '/move', [ ], $server, [ ], $request);
        }

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals(
            $this->client->getResponse()->getStatusCode(),
            400
        );
    }

    /** @test */
    public function gameShouldStartFromPlayerOne(): void
    {
        $this->client->request(
            'POST',
            '/move',
            [ ],
            [
                'headers' => [
                    'Content-type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ],
            [],
            json_encode([
                'game_id' => '12345678901234567890123456789012',
                'player_number' => 2,
                'position' => 2,
            ])
        );
        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('game should start from player 1', $content['message']);
        $this->assertEquals($this->client->getResponse()->getStatusCode(), 400);
    }

    /** @test */
    public function playerCanPlayTwice(): void
    {
        $gameId = '1234567890123456789012';
        $this->games->persistGame(new Board($gameId));

        $this->client->request(
            'POST',
            '/move',
            [ ],
            [
                'headers' => [
                    'Content-type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ],
            [ ],
            json_encode([
                'game_id' => $gameId,
                'player_number' => 1,
                'position' => 1,
            ])
        );

        $this->client->request(
            'POST',
            '/move',
            [ ],
            [
                'headers' => [
                    'Content-type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ],
            [ ],
            json_encode([
                'game_id' => $gameId,
                'player_number' => 1,
                'position' => 2,
            ])
        );

        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('same player cant move twice', $content['message']);
        $this->assertEquals($this->client->getResponse()->getStatusCode(), 400);
    }

    /** @test */
    public function cantMoveIntoAlreadyOccupiedTile(): void
    {
        $gameId = '1234567890123456789012';
        $this->games->persistGame(new Board($gameId));

        $this->client->request(
            'POST',
            '/move',
            [ ],
            [
                'headers' => [
                    'Content-type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ],
            [ ],
            json_encode([
                'game_id' => $gameId,
                'player_number' => 1,
                'position' => 1,
            ])
        );

        $this->client->request(
            'POST',
            '/move',
            [ ],
            [
                'headers' => [
                    'Content-type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ],
            [ ],
            json_encode([
                'game_id' => $gameId,
                'player_number' => 2,
                'position' => 1,
            ])
        );

        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('position is already occupied', $content['message']);
        $this->assertEquals($this->client->getResponse()->getStatusCode(), 400);
    }

    /** @test */
    public function cantMoveAgainAfterOneWinnerIsDeclared(): void
    {
        $gameId = '1234567890123456789012';
        $this->games->persistGame(new Board($gameId));

        $server = [
            'headers' => [
                'Content-type' => 'application/json',
                'Accept' => 'application/json',
            ]
        ];

        $moves = [[1, 1], [2, 3], [1, 0], [2, 4], [1, 2]];

        foreach ($moves as $move) {
            $request = json_encode([
                'game_id' => $gameId,
                'player_number' => $move[0],
                'position' => $move[1],
            ]);

            $this->client->request('POST', '/move', [ ], $server, [ ], $request);
        }

        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('player 1', $content['winner']);

        $request = json_encode([
            'game_id' => $gameId,
            'player_number' => 2,
            'position' => 5,
        ]);
        $this->client->request('POST', '/move', [ ], $server, [ ], $request);

        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals('game is closed', $content['message']);
        $this->assertEquals($this->client->getResponse()->getStatusCode(), 400);
    }
}
