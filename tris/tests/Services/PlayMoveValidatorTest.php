<?php

namespace App\Tests\Services;

use PHPUnit\Framework\TestCase;
use App\Services\PlayMoveValidator;
use Symfony\Component\HttpFoundation\Request;

class PlayMoveValidatorTest extends TestCase
{
    private Request $request;

    /** @test */
    public function checkIfRequestIsEmptyOrNot(): void
    {
        $this->request = $this
            ->getMockBuilder(Request::class)
            ->getMock();

        $this->request->expects($this->once())
            ->method('getContent')
            ->willReturn('');

        $service = new PlayMoveValidator();
        $service->setRequest($this->request);
        $this->assertSame(true, $service->isRequestEmpty());
        $this->assertSame('', $service->getResponse());
        $this->assertSame(false, $service->isRequestValid());
        $this->assertSame('empty request', $service->getErrorMessage());
    }

    /** @test */
    public function checkIfValidRequestContainsAllFields(): void
    {
        $response = json_encode([
            'NON' => 'EMPTY',
        ]);

        $this->request = $this
            ->getMockBuilder(Request::class)
            ->getMock();

        $this->request->expects($this->once())
            ->method('getContent')
            ->willReturn($response);

        $service = new PlayMoveValidator();
        $service->setRequest($this->request);
        $this->assertSame(false, $service->isRequestEmpty());
        $this->assertSame($response, $service->getResponse());
        $this->assertSame(false, $service->isRequestValid());
        $this->assertSame('game_id is missing', $service->getErrorMessage());
    }

    /** @test */
    public function completeRequestGiveNoErrorMessage(): void
    {
        $response = json_encode([
            'game_id' => '09876543211234567890098765432112',
            'player_number' => rand(1, 2),
            'position' => rand(1, 9),
        ]);

        $this->request = $this
            ->getMockBuilder(Request::class)
            ->getMock();

        $this->request->expects($this->once())
            ->method('getContent')
            ->willReturn($response);

        $service = new PlayMoveValidator();
        $service->setRequest($this->request);
        $this->assertSame(false, $service->isRequestEmpty());
        $this->assertSame($response, $service->getResponse());
        $this->assertSame(true, $service->isRequestValid());
        $this->assertSame(null, $service->getErrorMessage());
    }

    /** @test */
    public function wrongPositionCausesIvalidRequest(): void
    {
        $response = json_encode([
            'game_id' => '09876543211234567890098765432112',
            'player_number' => rand(1, 2),
            'position' => 42,
        ]);

        $this->request = $this
            ->getMockBuilder(Request::class)
            ->getMock();

        $this->request->expects($this->once())
            ->method('getContent')
            ->willReturn($response);

        $service = new PlayMoveValidator();
        $service->setRequest($this->request);
        $this->assertSame(false, $service->isRequestValid());
        $this->assertSame('position must be a number between 0 and 8', $service->getErrorMessage());
    }

    /** @test */
    public function idWithInvalidLengthMakesRequestInvalid(): void
    {
        $response = json_encode([
            'game_id' => '0985432112',
            'player_number' => rand(1, 2),
            'position' => 42,
        ]);

        $this->request = $this
            ->getMockBuilder(Request::class)
            ->getMock();

        $this->request->expects($this->once())
            ->method('getContent')
            ->willReturn($response);

        $service = new PlayMoveValidator();
        $service->setRequest($this->request);
        $this->assertSame(false, $service->isRequestValid());
        $this->assertSame('game_id is too short', $service->getErrorMessage());
    }
}
