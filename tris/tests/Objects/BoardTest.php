<?php

namespace App\Tests\Objects;

use PHPUnit\Framework\TestCase;
use App\Objects\Board;
use App\Objects\Move;
use App\Exceptions\WrongPositionException;
use App\Exceptions\WrongPlayerException;
use App\Exceptions\UnavailableMoveException;
use App\Exceptions\WrongStartException;

class BoardTest extends TestCase
{
    /** @test */
    public function containsNineElements(): void
    {
        $board = new Board('1234567890123456789012');
        $this->assertEquals(9, $board->numberOfTiles());
        $this->assertEquals(9, $board->unselectedTiles());
        $this->assertEquals('nobody', $board->winner());
    }

    /** @test */
    public function decreaseSelectableItemsAfterTileMark(): void
    {
        $board = new Board('1234567890123456789012');
        $board->mark(new Move(3, 1));

        $this->assertEquals(8, $board->unselectedTiles());
        $this->assertEquals('nobody', $board->winner());
    }

    /** @test */
    public function detectNextPlayer(): void
    {
        $board = new Board('1234567890123456789012');
        $this->assertEquals(1, $board->nextPlayer());
        $board->mark(new Move(1, 1));
        $this->assertEquals(2, $board->nextPlayer());
        $this->assertEquals('nobody', $board->winner());
    }

    /** @test */
    public function cantStartFromPlayerTwo(): void
    {
        $this->expectException(WrongStartException::class);

        $board = new Board('1234567890123456789012');
        $board->mark(new Move(3, 2));
        $this->assertEquals('nobody', $board->winner());
    }

    /** @test */
    public function cantMoveSamePlayerTwice(): void
    {
        $this->expectException(WrongPlayerException::class);

        $board = new Board('1234567890123456789012');
        $board->mark(new Move(3, 1));
        $board->mark(new Move(4, 1));
    }

    /** @test */
    public function cantRepeatMoveInSameGame(): void
    {
        $this->expectException(WrongPositionException::class);

        $board = new Board('1234567890123456789012');
        $board->mark(new Move(1, 1));
        $board->mark(new Move(1, 2));
        $this->assertEquals('nobody', $board->winner());
    }

    /**
     * @test
     * @dataProvider games
     */
    public function playerOneWin($moves, $winner): void
    {
        $board = new Board('1234567890123456789012');

        foreach ($moves as $move => $foo) {
            $board->mark(new Move($foo[0] , $foo[1]));
        }

        $this->assertEquals($winner, $board->winner());
    }

    public function games()
    {
        return [
            ['moves' => [[0, 1], [3, 2], [1, 1], [4, 2], [2, 1]], 'player 1'],
            ['moves' => [[3, 1], [1, 2], [4, 1], [6, 2], [5, 1]], 'player 1'],
            ['moves' => [[6, 1], [1, 2], [7, 1], [5, 2], [8, 1]], 'player 1'],
            ['moves' => [[2, 1], [6, 2], [1, 1], [7, 2], [5, 1], [8, 2]], 'player 2'],
            ['moves' => [[0, 1], [6, 2], [3, 1], [5, 2], [4, 1]], 'player 1'],
            ['moves' => [[1, 1], [6, 2], [4, 1], [5, 2], [7, 1]], 'player 1'],
            ['moves' => [[2, 1], [6, 2], [5, 1], [1, 2], [8, 1]], 'player 1'],
            ['moves' => [[0, 1], [6, 2], [4, 1], [1, 2], [8, 1]], 'player 1'],
            ['moves' => [[2, 1], [5, 2], [4, 1], [1, 2], [6, 1]], 'player 1'],
        ];
    }

    /** @test */
    public function detectLastPlayer(): void
    {
        $board = new Board('1234567890123456789012');

        $board->mark(new Move(0, 1));
        $this->assertEquals(1, $board->getLastPlayer());

        $board->mark(new Move(3, 2));
        $this->assertEquals(2, $board->getLastPlayer());

        $board->mark(new Move(1, 1));
        $board->mark(new Move(4, 2));

        $board->mark(new Move(2, 1));
        $this->assertEquals(1, $board->getLastPlayer());
    }

    /** @test */
    public function cantMarkWheneverAWinnerAlreadyExists(): void
    {
        $this->expectException(UnavailableMoveException::class);

        $board = new Board('1234567890123456789012');

        $board->mark(new Move(0, 1));
        $board->mark(new Move(3, 2));
        $board->mark(new Move(1, 1));
        $board->mark(new Move(4, 2));
        $board->mark(new Move(2, 1));
        $board->mark(new Move(8, 2));
    }

    /**
     * @test
     * @dataProvider nobodyWins
     */
    public function noWinner($moves, $winner): void
    {
        $board = new Board('1234567890123456789012');

        foreach ($moves as $move => $foo) {
            $board->mark(new Move($foo[0] , $foo[1]));
        }

        $this->assertEquals($winner, $board->winner());
        $this->assertSame(0, $board->unselectedTiles());
    }

    public function nobodyWins()
    {
        return [
            ['moves' => [
                [0, 1],
                [1, 2],
                [2, 1],
                [4, 2],
                [3, 1],
                [6, 2],
                [5, 1],
                [8, 2],
                [7, 1],
            ], 'nobody'],
        ];
    }
}
