<?php

namespace App\Objects;

use App\Exceptions\WrongPlayerException;
use App\Exceptions\WrongPositionException;
use App\Exceptions\UnavailableMoveException;
use App\Exceptions\WrongStartException;

class Board
{
    private $tiles = [
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
    ];

    public function __construct(
        public string $boardId
    ) { }

    public function id()
    {
        return $this->boardId;
    }

    public function numberOfTiles()
    {
        return count($this->tiles);
    }

    public function unselectedTiles()
    {
        $available = $this->numberOfTiles();

        foreach ($this->tiles as $position => $player) {
            if ($player !== null) {
                $available--;
            }
        }

        return $available;
    }

    public function mark(Move $move)
    {
        if ($this->winner() !== 'nobody') {
            throw new UnavailableMoveException();
        }

        if ($this->tiles[$move->position] !== null) {
            throw new WrongPositionException();
        }

        if ($move->player() === 2 && $this->unselectedTiles() === 9) {
            throw new WrongStartException();
        }

        if (
            $move->player() !== $this->nextPlayer()
        ) {
            throw new WrongPlayerException();
        }

        $this->tiles[$move->position()] = $move->player();
    }

    public function showTiles()
    {
        return $this->tiles;
    }

    public function nextPlayer()
    {
        return $this->unselectedTiles() % 2 === 0 ? 2 : 1;
    }

    public function winner()
    {
        $combinations = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],

            [0, 3, 4],
            [1, 4, 7],
            [2, 5, 8],

            [0, 4, 8],
            [2, 4, 6],
        ];

        foreach ($combinations as $tris) {
            if (
                $this->tiles[$tris[0]] == $this->getLastPlayer()
                && $this->tiles[$tris[1]] == $this->getLastPlayer()
                && $this->tiles[$tris[2]] == $this->getLastPlayer()
            ) {
                return 'player ' . $this->getLastPlayer();
            }
        }

        return 'nobody';
    }

    public function getLastPlayer()
    {
        return $this->nextPlayer() === 1 ? 2 : 1;
    }
}
