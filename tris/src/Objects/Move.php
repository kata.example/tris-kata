<?php

namespace App\Objects;

class Move
{
    public function __construct(
        public int $position,
        public int $player,
    ) { }

    public function position()
    {
        return $this->position;
    }

    public function player()
    {
        return $this->player;
    }
}
