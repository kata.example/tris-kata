<?php

namespace App\Services;

use App\Objects\Board;
use App\Objects\Move;
use App\Services\MovesManager;

class Filler
{
    public function __construct(
        public MovesManager $moves
    ) {}

    public function fillBoard(
        Board $board
    ) {
        $response = $this
            ->moves
            ->findAllMovesByBoardId($board->id());

        foreach ($response as $move) {
            $board->mark(new Move(
                $move['position'],
                $move['player'],
            ));
        }
    }
}
