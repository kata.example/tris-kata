<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\Request;

class PlayMoveValidator
{
    private $request;

    private $content;

    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    public function getResponse()
    {
        if ($this->content === null) {
            $this->content = $this->request->getContent();
        }

        return $this->content;
    }

    public function getJsonResponse()
    {
        return json_decode(
            $this->getResponse(),
            true
        );
    }

    public function isRequestValid()
    {
        $decoded = json_decode($this->getResponse(), true);

        return

            isset($decoded['game_id'])
            && isset($decoded['player_number'])
            && isset($decoded['position'])

            && (
                $decoded['player_number'] === 1
                || $decoded['player_number'] === 2
            )

            && (
                $decoded['position'] >= 0
                && $decoded['position'] <= 8
            )

        ;
    }

    public function isRequestEmpty()
    {
        return $this->getResponse() === '';
    }

    public function getErrorMessage()
    {
        $json = json_decode($this->getResponse(), true);

        if ($this->isRequestEmpty()) {
            return 'empty request';
        }

        if (!isset($json['game_id'])) {
            return 'game_id is missing';
        }

        if (!isset($json['position'])) {
            return 'position is missing';
        }

        if (!isset($json['player_number'])) {
            return 'player number is missing';
        }

        if (strlen($json['game_id']) < 32) {
            return 'game_id is too short';
        }

        if (
            $json['player_number'] < 1
            || $json['player_number'] > 2
        ) {
            return 'player number can be 1 or 2';
        }

        if (
            $json['position'] < 0
            || $json['position'] > 8
        ) {
            return 'position must be a number between 0 and 8';
        }
    }
}
