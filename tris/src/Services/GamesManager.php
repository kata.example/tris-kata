<?php

namespace App\Services;

use Doctrine\DBAL\Connection;
use App\Objects\Board;

class GamesManager
{
    public function __construct(
        public Connection $conn
    ) {}

    public function contains(Board $board): bool
    {
        $stmt = $this->conn->prepare('select count(*) num from games where game_id = :game_id');
        $stmt->bindValue('game_id', $board->id());
        $stmt->execute();
        $response = $stmt->fetchAssociative();
        return $response['num'] === '1';
    }

    public function persistGame(Board $board)
    {
        $stmt = $this->conn->prepare('insert into games (game_id) values (:game_id);');
        $stmt->bindValue('game_id', $board->id());
        $stmt->execute();
    }

    public function numOfRowByBoard($publicId)
    {
        $stmt = $this->conn->prepare('select count(*) as num from games where game_id = :board');
        $stmt->bindValue('board', $publicId);
        $stmt->execute();
        $response = $stmt->fetchAssociative();
        return $response['num'];
    }
}

