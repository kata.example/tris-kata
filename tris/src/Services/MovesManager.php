<?php

namespace App\Services;

use Doctrine\DBAL\Connection;

class MovesManager
{
    public function __construct(
        public Connection $conn
    ) {}

    public function findAllMovesByBoardId($boardId)
    {
        $stmt = $this->conn->prepare('
            select
            game_id,
            position,
            player
            from moves
            where game_id = :game_id
        ');
        $stmt->bindValue('game_id', $boardId);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function persistMove($req)
    {
        $stmt = $this->conn->prepare('
            insert into moves (
                game_id,
                position,
                player
            ) values (
                :game_id,
                :position,
                :player
            )
        ');
        $stmt->bindValue('game_id', $req['game_id']);
        $stmt->bindValue('position', $req['position']);
        $stmt->bindValue('player', $req['player_number']);
        $stmt->execute();
    }
}
