<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Services\Filler;
use App\Objects\Board;

class ShowGameController extends AbstractController
{
    #[Route('/show/game', name: 'show_game')]
    public function index(
        Request $request,
        Filler $filler
    ): Response {
        $gameId = $request->query->get('game_id');

        $board =  new Board($gameId);

        $filler->fillBoard($board);

        $response = new JsonResponse([
            'board' => $board->showTiles(),
            'winner' => $board->winner(),
        ], 200);

        $response->setEncodingOptions($response->getEncodingOptions() | JSON_PRETTY_PRINT);

        return $response;
    }
}
