<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\DBAL\Connection;

class StartGameController extends AbstractController
{
    #[Route('/start', name: 'start', methods: ['POST'])]
    public function index(
        Connection $conn,
    ): Response
    {
        $randomString = '';
        $key = '0987654321qwertyuioplkhfdsazxcvbnm';
        for ($i = 0; $i < 32; $i++) {
            $randomString .= $key[rand(0, strlen($key) - 1)];
        }

        $stmt = $conn->prepare('insert into games (game_id) values (:game_id);');
        $stmt->bindValue('game_id', $randomString);
        $stmt->execute();

        $response = new JsonResponse([
            'game_id' => $randomString,
        ], 201);

        $response->setEncodingOptions($response->getEncodingOptions() | JSON_PRETTY_PRINT);

        return $response;
    }
}
