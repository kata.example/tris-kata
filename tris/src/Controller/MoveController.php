<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\DBAL\Connection;
use App\Services\PlayMoveValidator;
use App\Objects\Board;
use App\Objects\Move;
use App\Exceptions\WrongPlayerException;
use App\Exceptions\UnavailableMoveException;
use App\Exceptions\WrongPositionException;
use App\Exceptions\WrongStartException;
use App\Services\MovesManager;
use App\Services\GamesManager;
use App\Services\Filler;

class MoveController extends AbstractController
{
    #[Route('/move', name: 'move', methods: ['POST'])]
    public function index(
        Request $request,
        PlayMoveValidator $validator,
        Connection $conn,
        MovesManager $movesManager,
        GamesManager $gamesManager,
        Filler $filler,
    ): Response
    {
        $validator->setRequest($request);

        if (!$validator->isRequestValid()) {
            return new JsonResponse([
                'message' => $validator->getErrorMessage(),
            ], 400);
        }

        $req = $validator->getJsonResponse();

        $board = new Board($req['game_id']);

        $filler->fillBoard($board);

        try {
            $board->mark(new Move($req['position'], $req['player_number']));
        } catch (WrongStartException $E) {
            return new JsonResponse([
                'message' => 'game should start from player 1',
            ], 400);
        } catch (WrongPositionException $E) {
            return new JsonResponse([
                'message' => 'position is already occupied',
            ], 400);
        } catch (UnavailableMoveException $E) {
            return new JsonResponse([
                'message' => 'game is closed'
            ], 400);
        } catch (WrongPlayerException $E) {
            return new JsonResponse([
                'message' => 'same player cant move twice'
            ], 400);
        }

        if (!$gamesManager->contains($board)) {
            return new JsonResponse([
                'message' => 'game id not found'
            ], 400);
        }

        $movesManager->persistMove($req);

        $response = new JsonResponse([
            'board' => $board->showTiles(),
            'winner' => $board->winner(),
        ], 200);

        $response->setEncodingOptions($response->getEncodingOptions() | JSON_PRETTY_PRINT);

        return $response;
    }
}
