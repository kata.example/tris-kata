<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SwitchoTest extends WebTestCase
{
    protected $client;

    static $container;

    protected $doctrine;

    protected $connection;

    public function setUp(): void
    {
        $this->client = static::createClient();

        self::$container = static::getContainer();
        $this->doctrine = self::$container->get('doctrine');
        $this->connection = $this->doctrine->getConnection();

        $this->games = self::$container->get(\App\Services\GamesManager::class);

        $stmt = $this->connection->prepare('delete from games');
        $stmt->execute();

        $stmt = $this->connection->prepare('delete from moves');
        $stmt->execute();
    }
}
